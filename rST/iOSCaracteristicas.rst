|imagen de cabecera|

`Android <index.html>`__ | `iOS <iOSIntroduccion.html>`__ |
`Blackberry <BlackberryIntroduccion.html>`__ |
`Conclusión <Conlusion.html>`__ | `Webgrafía <WebGrafia.html>`__

`Introducción <iOSIntroduccion.html>`__ |
`Caracteristicas <iOSCaracteristicas.html>`__

iOS: Características
====================

A continuación procedemos a citar algunas de las muchas características
de este sistema operativo:

-  Pantalla: citar el SpringBoard y el Dock. El primero es el
   "escritorio" donde se encuentran los iconos, mientras que el Dock es
   una barra inferior que aparece al dejar pulsado el botón de inicio y
   es donde se puede ver las aplicaciones que se están ejecutando
-  Carpetas: al arrastrar una aplicación junto a otra automáticamente se
   crea una carpeta con ambas aplicaciones. Esto nos permite tener
   organizadas nuestras aplicaciones, lo cual se agradece cuando tenemos
   gran cantidad de software instalado.
-  Notificaciones: aparece cuando deslizamos el dedo desde la parte
   superior del movil hacia abajo y nos proporciona información sobre
   aplicaciones en ejecución, mensajes que recibamos, alertas, etc..
-  Aplicaciones: como es de esperar hay aplicaciones para casi cualquier
   propósito. Todas las aplicaciones son descargables desde la llamada
   AppStore que es similar al Google Play para Android.

**Autor:**\ *Daniel Jesús Umpiérrez Del Río* \| **Fecha de
entrega:**\ *13/02/2014*

.. |imagen de cabecera| image:: imagenes/cabecera.png
