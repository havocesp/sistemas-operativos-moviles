|imagen de cabecera|

`Android <index.html>`__ | `iOS <iOSIntroduccion.html>`__ |
`Blackberry <BlackberryIntroduccion.html>`__ |
`Conclusión <Conlusion.html>`__ | `Webgrafía <WebGrafia.html>`__

`Introducción <BlackberryIntroduccion.html>`__ |
`Caracteristicas <BlackberryCaracteristicas.html>`__

Blacberry: Introducción
=======================

|Imagen blackberry|

BlackBerry es una compañía canadiense que fabrica smartphones desde
1999, que tienen como característica el llevar integrados un teclado
tipo QWERTY completo.

Estos smartphones usan su propio sistema operativo llamado Blackberry
OS. Blackberry consiguió ganar una gran cuota de mercado en entornos
profesionales hace algunos años gracias a gran compatibilidad como
cliente de correo electrónico a su servicio de mensajería PIN.

Hoy en día ha pasado a un tercer lugar en cuanto a numero de usuarios.
Ha sido superado tanto por Android como por iOS de Apple.

**Autor:**\ *Daniel Jesús Umpiérrez Del Río* \| **Fecha de
entrega:**\ *13/02/2014*

.. |imagen de cabecera| image:: imagenes/cabecera.png
.. |Imagen blackberry| image:: imagenes/blackberry.png
