|imagen de cabecera|

`Android <index.html>`__ | `iOS <iOSIntroduccion.html>`__ |
`Blackberry <BlackberryIntroduccion.html>`__ |
`Conclusión <Conlusion.html>`__ | `Webgrafía <WebGrafia.html>`__

`Introducción <iOSIntroduccion.html>`__ |
`Caracteristicas <iOSCaracteristicas.html>`__

iOS: Introducción
=================

|Imagen apple|

iOS es el sistema operativo para dispositivos móviles de la compañía
Apple. En un primer momento, fue llamado iPhoneOS, por que fue diseñado
para ser el sistema operativo del smartphone iPhone.

No fué hasta junio de 2010 que paso a denominarse iOS ya que paso a ser el
sistema operativo de otros dispositivos de Apple, como puede ser la
tablet iPad.

En junio de 2013 fue presentada la versión 7 de iOS, que es la que
actualmente se encuentra en el mercado. Esta versión trae como principal
novedad una nueva interfaz gráfica.

Actualmente tiene una alta cuota de mercado aunque muy por debajo del
sistema operativo de Google, Android. Aun así es el segundo sistema
operativo para móviles más usado aunque va decreciendo desde la
aparición del iPhone 5 que parece que no a calado hondo entre los
usuarios.

**Autor:**\ *Daniel Jesús Umpiérrez Del Río* \| **Fecha de
entrega:**\ *13/02/2014*

.. |imagen de cabecera| image:: imagenes/cabecera.png
.. |Imagen apple| image:: imagenes/apple.jpg
