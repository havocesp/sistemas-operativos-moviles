|imagen de cabecera|

`Android <index.html>`__ | `iOS <iOSIntroduccion.html>`__ |
`Blackberry <BlackberryIntroduccion.html>`__ |
`Conclusión <Conlusion.html>`__ | `Webgrafía <WebGrafia.html>`__

`Introducción <BlackberryIntroduccion.html>`__ |
`Caracteristicas <BlackberryCaracteristicas.html>`__

Blackberry: Características
===========================

Procemos a hablar de algunas de las características del sistema
operativo de Blackberry:

-  El sistema permite la multitarea
-  Proporciona soporte para varios dispositivos de entrada, como
   trackballs, touchpad, pantallas táctiles, etc..
-  Desde sus primeras versiones de 1999 ya permitía acceso web y a
   correo electrónico Exchange y Lotus.
-  Tiene aplicaciones de calendario, de notas, contactos, tareas, etc.
-  Identifica en su versión Enterprise a cada usuario con un PIN único
   para el acceso a servidores de empresas.
-  Tiene un SDK para facilitar el desarrollo de aplicaciones para
   Blackberry

Estos smartphones usan su propio sistema operativo llamado Blackberry
OS. Blackberry consiguió ganar una gran cuota de mercado en entornos
profesionales hace algunos años gracias a gran compatibilidad como
cliente de correo electrónico a su servicio de mensajería PIN.

**Autor:**\ *Daniel Jesús Umpiérrez Del Río* \| **Fecha de
entrega:**\ *13/02/2014*

.. |imagen de cabecera| image:: imagenes/cabecera.png
