|imagen de cabecera|

`Android <index.html>`__ | `iOS <iOSIntroduccion.html>`__ |
`Blackberry <BlackberryIntroduccion.html>`__ |
`Conclusión <Conlusion.html>`__ | `Webgrafía <WebGrafia.html>`__

`Introducción <index.html>`__ | `Historia <AndroidHistoria.html>`__ |
`Caracteristicas <AndroidCaracteristicas.html>`__ |
`Arquitectura <AndroidArquitectura.html>`__ | `Diseño y
desarrollo <AndroidDesarrollo.html>`__

Webgrafía
=========

**Android:**

-  `http://es.wikipedia.org/wiki/Android <http://es.wikipedia.org/wiki/Android>`__
-  `http://www.unocero.com/2013/09/23/la-historia-de-android/ <http://www.unocero.com/2013/09/23/la-historia-de-android/>`__

**iOS:**

-  `http://es.wikipedia.org/wiki/IOS <http://es.wikipedia.org/wiki/IOS>`__
-  `http://es.wikipedia.org/wiki/IOS\_7 <http://es.wikipedia.org/wiki/IOS_7>`__

**Blackberry:**

-  `http://es.wikipedia.org/wiki/BlackBerry\_OS <http://es.wikipedia.org/wiki/BlackBerry_OS>`__
-  `http://es.wikipedia.org/wiki/BlackBerry <http://es.wikipedia.org/wiki/BlackBerry>`__

**Autor:**\ *Daniel Jesús Umpiérrez Del Río* \| **Fecha de
entrega:**\ *13/02/2014*

.. |imagen de cabecera| image:: imagenes/cabecera.png
