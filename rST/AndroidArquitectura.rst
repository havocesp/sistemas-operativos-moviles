|imagen de cabecera|

`Android <index.html>`__ | `iOS <iOSIntroduccion.html>`__ |
`Blackberry <BlackberryIntroduccion.html>`__ |
`Conclusión <Conlusion.html>`__ `Webgrafía <WebGrafia.html>`__ 

`Introducción <index.html>`__ `Historia <AndroidHistoria.html>`__ |
`Caracteristicas <AndroidCaracteristicas.html>`__ |
`Arquitectura <AndroidArquitectura.html>`__ | `Diseño y
desarrollo <AndroidDesarrollo.html>`__

Android: Arquitectura
=====================

Android cuenta con 5 componentes principales que son:

-  **Aplicaciones:** incluye un buen número de aplicaciones (escritas en
   Java) como por ejemplo:

   -  Cliente de correo electrónico
   -  Programa de SMS
   -  Software de calendario
   -  Mapas (Basado en Google Maps)
   -  Navegador web y de ficheros
   -  Administración de contactos

-  **Marco de trabajo de aplicaciones:** las aplicaciones pueden
   reutilizar funcionalidades de otras y viceversa lo que facilita la
   reutilización de código.
-  **Bibliotecas:** incluye una gran biblioteca utilizable por los
   programadores como pueden ser SQlite, bibliotecas 3D, etc…
-  **Entorno de ejecución Android:** basa su potencia en la máquina
   virtual Dalvik Java, que esta optimizada para hacer un uso mínimo de
   recursos.
-  **Núcleo Linux:** depende de Linux para temas como la seguridad,
   gestión de procesos, pila de red, controladores, etc...

**Autor:**\ *Daniel Jesús Umpiérrez Del Río* \| **Fecha de
entrega:**\ *13/02/2014*

.. |imagen de cabecera| image:: imagenes/cabecera.png
