|imagen de cabecera|

`Android <index.html>`__ | `iOS <iOSIntroduccion.html>`__ |
`Blackberry <BlackberryIntroduccion.html>`__ |
`Conclusión <Conlusion.html>`__ | `Webgrafía <WebGrafia.html>`__

`Introducción <index.html>`__ | `Historia <AndroidHistoria.html>`__ |
`Caracteristicas <AndroidCaracteristicas.html>`__ |
`Arquitectura <AndroidArquitectura.html>`__ | `Diseño y
desarrollo <AndroidDesarrollo.html>`__

Android: Características
========================

Son muchas las características que podríamos citar sobre ese sistema
operativo, pero solo vamos a hablar de las más relevantes:

-  **Conectividad:** como no podría ser menos para un sistema operativo
   ideado para dispositivos móviles, cuenta con muchísimas maneras de
   comunicarse como GPS, Bluetooth, Wifi, IR, datos móviles de todo
   tipo, etc…
-  **Soporte multimedia:** este es otro de los puntos fuertes de
   Android, soporta una variadísima cantidad de formatos tanto de vídeo,
   imágenes, sonido, etc..
-  **Soporte de hardware:** es capaz de soportar todo tipo de
   dispositivos como pueden ser las cámaras de fotos y video, GPS,
   acelerómetros, giroscopios, sensores, etc…
-  **Google Play:** es una plataforma para adquisición de software para
   el sistema operativo tanto gratuito como comercial. Intenta mediante
   una serie de normas para los desarrolladores, evitar el
   software malintencionado por lo que proporciona algo de seguridad al
   usuario.
-  **Entorno de desarrollo:** aunque Google está desarrollando una
   plataforma específica para desarrollo, de momento existe un plugin
   para el entorno Eclipse que cumple bastante bien con el objetivo de
   facilitar el desarrollo de software para el sistema operativo.

**Autor:**\ *Daniel Jesús Umpiérrez Del Río* \| **Fecha de
entrega:**\ *13/02/2014*

.. |imagen de cabecera| image:: imagenes/cabecera.png
