|imagen de cabecera|

`Android <index.html>`__ `iOS <iOSIntroduccion.html>`__
`Blackberry <BlackberryIntroduccion.html>`__
`Conclusión <Conlusion.html>`__ `Webgrafía <WebGrafia.html>`__

`Introducción <index.html>`__ `Historia <AndroidHistoria.html>`__
`Caracteristicas <AndroidCaracteristicas.html>`__
`Arquitectura <AndroidArquitectura.html>`__ `Diseño y
desarrollo <AndroidDesarrollo.html>`__

Android: historia
=================

|Imagen Codigo|

Inicialmente fue desarrollado por una compañía del mismo nombre que el
sistema operativo. Esta compañía, Android Inc., fue fundada en 2003 en
california pero en 2005 fue adquirida por Google, lo que dió lugar a que
muchos de los fundadores de Android fueran contratados por Google como
desarrolladores.

Fue anunciado en noviembre de 2007, en 2010 ya tenía una cuota de
mercado del 43,6% y en 2011 ya era del 50,9% lo que da una visión de
cómo se ha ido incrementando su uso, siendo hoy en día bastante más alta
esta cifra de cuota de mercado para este sistema operativo.

**Autor:**\ *Daniel Jesús Umpiérrez Del Río* \| **Fecha de
entrega:**\ *13/02/2014*

.. |imagen de cabecera| image:: imagenes/cabecera.png
.. |Imagen Codigo| image:: imagenes/code.png
