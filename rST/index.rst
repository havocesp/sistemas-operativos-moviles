|Cabecera|

`Android <index.html>`__ | `iOS <iOSIntroduccion.html>`__ |
`Blackberry <BlackberryIntroduccion.html>`__ |
`Conclusión <Conlusion.html>`__ | `Webgrafía <WebGrafia.html>`__

`Introducción <index.html>`__ | `Historia <AndroidHistoria.html>`__ |
`Caracteristicas <AndroidCaracteristicas.html>`__ |
`Arquitectura <AndroidArquitectura.html>`__ | `Diseño y
desarrollo <AndroidDesarrollo.html>`__

Android: Introducción
=====================

|Logo Android|

Android es un sistema operativo diseñado para dispositivos móviles como
pueden ser teléfonos inteligentes (también llamados smartphones) o
tabletas.

Los últimos datos sobre la cuota de mercado de sistemas operativos en
este tipo de dispositivos lo sitúan en un primer lugar seguido de lejos
del sistema operativo para móviles de Apple, el iOS.

El primer dispositivo que salió al mercado con Android fue el
teléfono “Dream” de la compañía HTC en 2008.

Desde entonces no ha hecho más que aumentar su presencia en el mundo,
además Google es la compañía que está detrás de su desarrollo lo que le
augura un buen futuro.

**Autor:**\ *Daniel Jesús Umpiérrez Del Río* \| **Fecha de
entrega:**\ *13/02/2014*


.. |Logo Android| image:: ./imagenes/android.png
.. |Cabecera| image:: ./imagenes/cabecera.png