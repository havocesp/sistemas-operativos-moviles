|imagen de cabecera|

`Android <index.html>`__ | `iOS <iOSIntroduccion.html>`__ |
`Blackberry <BlackberryIntroduccion.html>`__ |
`Conclusión <Conlusion.html>`__ | `Webgrafía <WebGrafia.html>`__

`Introducción <index.html>`__ | `Historia <AndroidHistoria.html>`__ |
`Caracteristicas <AndroidCaracteristicas.html>`__ |
`Arquitectura <AndroidArquitectura.html>`__ | `Diseño y
desarrollo <AndroidDesarrollo.html>`__

Conclusión
==========

|Imagen conclusion|

Aunque los dispositivos móviles hace relativamente poco tiempo que se
encuentran entre nosotros, han avanzado con una rapidez extraordinaria
para adapatarse a los tiempos.

Podemos ver como Android, que hasta hace un par de años era un
desconocido para la gran mayoría de los usuarios de nuevas tecnologías,
ha captado el primer puesto en la carrera por la cuota de mercado, antes
ocupada por sistemas operativos como Symbian OS (del que no hemos
hablado en esta web por que ya esta prácticamente desaparecido),
haciendo que practicamente todo el mundo lo conozca a día de hoy. Esto
nos da una idea de lo rápido que evoluciona este mercado.

Por otro lado decir que el futuro de los sistemas operativos posiblemente
se encuentre en la de los dispositivos móviles, o al menos así lo
demuestra la bajada de ventas de PCs, tanto de sobremesa como
portátiles, en favor de los smartphones o tablets. Al tener acceso a
internet, servicios de mensajería y correo electrónico desde estos
dispositivos los usuarios no ven necesidad de usar PCs salvo que su
trabajo se lo exija.

**Autor:**\ *Daniel Jesús Umpiérrez Del Río* \| **Fecha de
entrega:**\ *13/02/2014*

.. |imagen de cabecera| image:: imagenes/cabecera.png
.. |Imagen conclusion| image:: imagenes/lista.png
