|imagen de cabecera|

`Android <index.html>`__ | `iOS <iOSIntroduccion.html>`__ |
`Blackberry <BlackberryIntroduccion.html>`__ |
`Conclusión <Conlusion.html>`__ | `Webgrafía <WebGrafia.html>`__

`Introducción <index.html>`__ | `Historia <AndroidHistoria.html>`__ |
`Caracteristicas <AndroidCaracteristicas.html>`__ |
`Arquitectura <AndroidArquitectura.html>`__ | `Diseño y
desarrollo <AndroidDesarrollo.html>`__

Android: Diseño y desarrollo
============================

|Imagen desarrollo|

Android es considera un modelo a seguir por desarrolladores. A
diferencia de otros sistemas como iOS de Apple o Windows Phone de
Microsoft, Android es software libre.

Se puede acceder a su código fuente sin restricciones. Las únicas
restricciones suelen venir de la mano de los fabricantes de hardware
sobre todo en cuanto al firmware de su dispositivo y el código de los
controladores para el mismo.

En un principio fue diseñado para usar con teclado convencional, pero
gracias a la gran flexibilidad de este sistema operativo y a que su
primer lanzamiento fue en un dispositivo móvil esta filosofía cambio
rápidamente.

**Autor:**\ *Daniel Jesús Umpiérrez Del Río* \| **Fecha de
entrega:**\ *13/02/2014*

.. |imagen de cabecera| image:: imagenes/cabecera.png
.. |Imagen desarrollo| image:: imagenes/regla.png
