README
======

**Autor**: Daniel Jesus Umpi�rrez Del R�o

**Fecha**: 13/02/2014

Ciclo formativo de grado superior de administraci�n de sistemas informaticos en red
-----------------------------------------------------------------------------------
**Asignatura**: Lenguaje de marcas

**Tarea**: 3

**Tema:** Sistemas operativos para dispositivos m�viles

Objetivo
--------
Aprender lenguajes de marcas sencillos y uso de bitbucket.

Conclusi�n
----------
Esta pr�ctica me ha servido para aprender sobre Git (Bash) para su uso con BitBucket.
Adem�s he aprendido olvidarme de mi antiguo sistema de maquetado por tablas y aprender
a maquetar en css una p�gina web.

Ademas he descubieto algunas herramientas como Pandoc que no conoc�a, que me permitieron
pasar r�pidamente de HTML a RestructuredText mediante una sencilla orden.

Por otro lado el me ayudo a conocer alguna caracter�stica m�s de RestructuredText consolidar 
el formato rST y repasar muchas etiquetas CSS y HTML que ten�a olvidadas.
